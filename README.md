### Microservice Poker Application

# AIM
To have a fully functional, cloud agnostic Texas Hold'em Game using a microservice architecture. 

Needs some work and tidying up, I'm sure there's a lot of best practises that I'm not coding to as I'm not familiar with them, I'm open to any improvement suggestions :)

# Stack
 - frontend - React Flux
 - lobby - Golang 
 - game-engine - Python
 - DB - PostgreSQL
 - Kubernetes orchestrator 
 - Docker
 - Gitlab CI/CD

# Note
Alot of the game engine code I wrote a year ago but I am refactoring it to be functional. original code on my [github](https://github.com/rossmcq/TexasHoldEm)
