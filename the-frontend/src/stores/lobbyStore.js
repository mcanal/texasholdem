import {EventEmitter} from 'events'
import Dispatcher from '../appDispatcher'
import actionTypes from '../actions/actionTypes'

const CHANGE_EVENT = "change";
let _games = [];

class gameStore extends EventEmitter{
    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback)
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback)
    }

    emitChange(){
        this.emit(CHANGE_EVENT);
    }

    getGames() {
        return _games;
    }

    getGameBySlug(slug) {
        return _games.find(game => game.slug === slug);
    }

}

const store = new gameStore();

Dispatcher.register(action => {
    switch(action.actionType) {
        // case actionTypes.CREATE_GAME:
        //     _games.push(action.game);
        //     store.emitChange();
        //     break;
        // case actionTypes.DELETE_GAME:
        //     debugger;
        //     _games = _games.filter(
        //         game => game.id !== parseInt(action.id, 10)
        //         );
        //     store.emitChange();
        //     break;
        // case actionTypes.UPDATE_GAME:
        //     _games = _games.map( game => 
        //         game.id === action.game.id ?
        //         action.game : game);
        //     store.emitChange();
        //     break;
        case actionTypes.LOAD_GAMES:
            _games = action.games
            store.emitChange();
            break;
        default:
            //Nada de nada
    }
})

export default store;