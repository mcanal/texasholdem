import { handleResponse, handleError } from "./apiUtils";
const BASE_LOBBY_API = process.env.NODE_ENV === "production" ? "http://localhost:30040" :"http://localhost:4000";
const LOBBY_API = BASE_LOBBY_API + "/games/v1/";

export function getGames() {
  return fetch(LOBBY_API)
    .then(handleResponse)
    .catch(handleError);
}

// export function getGameBySlug(slug) {
//   return fetch(baseUrl + "?slug=" + slug)
//     .then(response => {
//       if (!response.ok) throw new Error("Network response was not ok.");
//       return response.json().then(courses => {
//         if (courses.length !== 1) throw new Error("Game not found: " + slug);
//         return courses[0]; // should only find one course for a given slug, so return it.
//       });
//     })
//     .catch(handleError);
// }

export function createGame(userSub, gameType) {
  return fetch(LOBBY_API , {
    method: "POST",
    headers: { "content-type": "application/json" },
    body: JSON.stringify({
      userSub: userSub,
      gameType: gameType
    })
  })
    .then(handleResponse)
    .catch(handleError);
}

// export function deleteGame(courseId) {
//   return fetch(baseUrl + courseId, { method: "DELETE" })
//     .then(handleResponse)
//     .catch(handleError);
// }
