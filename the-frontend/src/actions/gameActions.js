import dispatcher from '../appDispatcher';
import * as lobbyApi from '../api/lobbyApi';
import actionTypes from './actionTypes';


export function loadGames() {
    return lobbyApi.getGames().then(games => {
        dispatcher.dispatch({
            actionType: actionTypes.LOAD_GAMES,
            games: games
        });
    });
}

// export function deleteGame(id) {
//     debugger;
//     return gameApi.deleteGame(id).then(() => {
//         dispatcher.dispatch({
//             actionType: actionTypes.DELETE_GAME,
//             id: id
//         });
//     });
// }
