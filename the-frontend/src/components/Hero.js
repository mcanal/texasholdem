import React, { useState, useEffect } from "react";
import { useAuth0 } from "@auth0/auth0-react";
import logo from "../assets/logo.svg";
import { Button } from "reactstrap";
import CurrentGamesList from "./CurrentGamesList"
import lobbyStore from "../stores/lobbyStore"
import { loadGames } from "../actions/gameActions"

function Hero () {
  const [games, setGames] = useState(lobbyStore.getGames());
  const TESTIF = process.env.NODE_ENV === "development" ? "http://localhost:4000" :"http://lobby-svc:80";
  const {
    user,
    isAuthenticated,
  } = useAuth0();

   //TODO: implement this
  function joinGame(userSub) {
    console.log(userSub + "Play wants to join game")
  }

  useEffect( () => {
    lobbyStore.addChangeListener(onChange);
    if (lobbyStore.getGames().length === 0) loadGames();
    return () => lobbyStore.removeChangeListener(onChange);
}, [])

  function onChange() {
    setGames(lobbyStore.getGames());
}

  return (<div className="text-center hero my-5">
    <img className="mb-3 app-logo" src={logo} alt="React logo" width="120" />
    <h1 className="mb-4">Poker Game!!</h1>
    <h2>Env - {process.env.NODE_ENV}</h2>
    <h2>Lobby API - {TESTIF}</h2>

    {!isAuthenticated && (
    <p className="lead">
      Log in to play poker This was updated on Dec 28th!! <a href="https://reactjs.org">React.js</a>
    </p>
    )}
    {isAuthenticated && (
      <p>Welcome {user.name}</p>)}

      <><Button
              id="qsCreateGameBtn"
              color="primary"
              className="btn-margin"
            >
                  Create Game!
    </Button>
    <CurrentGamesList games={games} joinGame={joinGame} /></>

  </div>)};

export default Hero;
