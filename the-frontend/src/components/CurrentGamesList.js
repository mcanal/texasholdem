import React from 'react'
import PropTypes from 'prop-types'
 
function CurrentGamesList(props) {
    return (
        <table className="table">
            <thead>
                <tr>
                    <th>Game Type</th>
                    <th>Capacity</th>
                    <th>Join</th>
                </tr>
            </thead>
            <tbody>
                {props.games.map( game => {
                    return (
                        <tr key={game.ID}>
                            <td>{game.GameType}</td>
                            <td>0/8</td>
                            <td>
                                <button 
                                className="btn btn-outline-success" 
                                onClick={() => {props.joinGame(game.ID)}}>
                                    Join
                                </button>
                            </td>
                            
                        </tr> );
                })}
            </tbody>
        </table>
    );
}

CurrentGamesList.propTypes = {
    joinGame: PropTypes.func.isRequired,
    games: PropTypes.arrayOf(
        PropTypes.shape({
            ID: PropTypes.string.isRequired,
            GameType: PropTypes.string.isRequired
    })).isRequired
}

export default CurrentGamesList