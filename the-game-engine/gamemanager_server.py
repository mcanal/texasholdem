import grpc
from concurrent import futures
import time
import logging
import gamemanager_pb2_grpc as pb2_grpc
import gamemanager_pb2 as pb2
import helpers as Tex


class GameManagerService(pb2_grpc.GameEventProcessorServicer):

    def __init__(self, *args, **kwargs):
        pass

    def CreateNewGame(self, request, context):
        '''
        Create a new game and return the game_id
        '''

        # get the string from the incoming request
        user = request.user
        gameID = str(Tex.create_game_and_add_player(user))
        result = {'gameID': gameID, 'gameCreated': True}
        logging.info(f'Newly created Game - {gameID}, creator - {user}')
        return pb2.CreateNewGameReply(**result)


def serve():

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    pb2_grpc.add_GameEventProcessorServicer_to_server(
        GameManagerService(), server)
    server.add_insecure_port('[::]:5000')
    server.start()
    logging.info('Game Engine Started')
    server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    serve()
