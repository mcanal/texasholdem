import grpc
import gamemanager_pb2_grpc as pb2_grpc
import gamemanager_pb2 as pb2


class GameManagerClient(object):
    """
    Client for gRPC functionality
    """

    def __init__(self):
        self.host = 'localhost'
        self.server_port = 5000

        # instantiate a channel
        self.channel = grpc.insecure_channel(
            '{}:{}'.format(self.host, self.server_port))

        # bind the client and the server
        self.stub = pb2_grpc.GameEventProcessorStub(self.channel)

    def get_url(self, user):
        """
        Client function to call the rpc for GetServerResponse
        """
        user = pb2.CreateNewGameRequest(user=user)
        print(f'{user}')
        return self.stub.CreateNewGame(user)


if __name__ == '__main__':
    client = GameManagerClient()
    result = client.get_url(user="ross")
    print(f'{result}')