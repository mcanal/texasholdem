package main

var emptyGames = []Game{}
var games = []Game{
	Game{
		ID:       "1",
		GameType: "Poker",
	},
}

//Game -
type Game struct {
	ID       string `json:"ID"`
	GameType string `json:"GameType"`
	Player1  string `json:"Player1"`
	Player2  string `json:"Player2"`
	Player3  string `json:"Player3"`
	Player4  string `json:"Player4"`
	Player5  string `json:"Player5"`
	Player6  string `json:"Player6"`
	Player7  string `json:"Player7"`
	Player8  string `json:"Player8"`
}

type createGameRequest struct {
	UserSub  string `json:"userSub"`
	GameType string `json:"GameType"`
}

type dbGamesRepo struct {
	games []Game
}

type gameRepository interface {
	addGame(game Game) (err error)
	getGames() []Game
}
