package main

import (
	"database/sql"
	"log"
	"net/http"
	"os"
	"time"

	_ "github.com/lib/pq"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

var repo dbGamesRepo
var postgresDb = os.Getenv("POSTGRES_DB")
var postgresUser = os.Getenv("POSTGRES_USER")
var postgresPw = os.Getenv("POSTGRES_PASSWORD")
var postgresHost = os.Getenv("POSTGRES_SERVICE_SERVICE_HOST")
var postgresPort = os.Getenv("POSTGRES_SERVICE_SERVICE_PORT")

//Database -Connection to PostGresDB
func Database(connString string) gin.HandlerFunc {
	db, err := sql.Open("postgres", connString)
	if err != nil {
		log.Fatal(err)
	}

	err = db.Ping()
	if err != nil {
		panic(err)
	}

	return func(c *gin.Context) {
		c.Set("DB", db)
		c.Next()
	}
}

func setUpGin() *gin.Engine {
	router := gin.Default()

	connStr := "user=" + postgresUser + " dbname=" + postgresDb + " password=" + postgresPw + " host=" + postgresHost + " port=" + postgresPort + " sslmode=disable"
	router.Use(Database(connStr))

	// Register handlers
	router.GET("/", func(context *gin.Context) {
		context.String(http.StatusOK, "POSTGRES_DB: "+postgresHost)
	})

	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"http://localhost:3000", "http://localhost:30030"},
		AllowMethods:     []string{"GET, PUT, POST, DELETE"},
		AllowHeaders:     []string{"Content-Type, Authorization"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		// AllowOriginFunc: func(origin string) bool {
		//     return origin == "https://github.com"
		// },
		MaxAge: 12 * time.Hour,
	}))

	// Game CRUD routes and handlers
	gameGroup := router.Group("games/v1")
	{
		gameGroup.GET("/", GetAll)
		// gameGroup.GET("/:id", Get)
		gameGroup.POST("/", Create)
		// gameGroup.PATCH("/:id", Update)
		// gameGroup.DELETE("/:id", Delete)
	}

	return router
}

func main() {

	router := setUpGin()
	router.Run(":4000")
}
