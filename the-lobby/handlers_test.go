package main

const (
	fakeUserSub  = "google-oauth2|107474287719355099999"
	fakeGameType = "Poker"
)

// func TestCreateMatchRespondsToBadData(t *testing.T) {
// 	router := setUpGin()

// 	w := httptest.NewRecorder()

// 	body1 := []byte("this is not valid json")
// 	body2 := []byte("{\"test\":\"this is valid json, but doesn't conform to server expectations.\"}")
// 	fmt.Println("Test1")

// 	// Send invalid JSON
// 	req, err := http.NewRequest("POST", "/games/v1/", bytes.NewBuffer(body1))
// 	if err != nil {
// 		t.Errorf("Error in creating POST request for createMatchHandler: %v", err)
// 	}
// 	req.Header.Add("Content-Type", "application/json")

// 	router.ServeHTTP(w, req)
// 	assert.Equal(t, 400, w.Code, "Sending invalid JSON should result in a bad request from server.")

// 	req2, err2 := http.NewRequest("POST", "/games/v1/", bytes.NewBuffer(body2))
// 	if err2 != nil {
// 		t.Errorf("Error in creating second POST request for invalid data on create match: %v", err2)
// 	}
// 	req2.Header.Add("Content-Type", "application/json")

// 	router.ServeHTTP(w, req)
// 	assert.Equal(t, 400, w.Code, "Sending valid JSON but with incorrect or missing fields should result in a bad request and didn't.")
// }

// func TestCreateMatch(t *testing.T) {
// 	router := setUpGin()

// 	w := httptest.NewRecorder()
// 	request := map[string]string{"userSub": fakeUserSub, "gameType": fakeGameType}
// 	data, err := json.Marshal(request)

// 	if err != nil {
// 		t.Errorf("Error in creating post request for createGameHandler %v", err)
// 	}
// 	b := bytes.NewBuffer(data)
// 	req, _ := http.NewRequest("POST", "/games/v1/", b)
// 	router.ServeHTTP(w, req)

// 	assert.Equal(t, 200, w.Code)
// 	assert.JSONEq(t, `{"message":"Game created","status":200}`, w.Body.String())
// }
