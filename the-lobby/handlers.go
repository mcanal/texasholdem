package main

import (
	geContext "context"
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	pb "gitlab.com/mcanal/texasholdem/the-lobby/protos"
	"google.golang.org/grpc"
)

func getStoreConnection() (*grpc.ClientConn, error) {
	host := os.Getenv("GAMEENGINE_SERVICE_SERVICE_HOST")
	if len(host) == 0 {
		host = "localhost"
	}
	return grpc.Dial(host+":5000", grpc.WithInsecure())
}

func playerJoinGame(userid string, gameID string, db *sql.DB) {

	stmt, err := db.Prepare(`update lobby.games
								set Player1 = case when (Player1 IS NULL) then $1 else Player1 end,
									Player2 = case when (Player1 IS NOT NULL) 
												and     (Player2 IS NULL) then $1 else Player2 end,
									Player3 = case when (Player1 IS NOT NULL) 
												and 	(Player2 IS NOT NULL)
												and     (Player3 IS NULL) then $1 else Player3 end,
									Player4 = case when (Player1 IS NOT NULL) 
												and 	(Player2 IS NOT NULL)
												and     (Player3 IS NOT NULL) 
												and     (Player4 IS NULL) then $1 else Player4 end,
									Player5 = case when (Player1 IS NOT NULL) 
												and 	(Player2 IS NOT NULL)
												and     (Player3 IS NOT NULL)
												and     (Player4 IS NOT NULL) 
												and     (Player5 IS NULL) then $1 else Player5 end,
									Player6 = case when (Player1 IS NOT NULL) 
												and 	(Player2 IS NOT NULL)
												and     (Player3 IS NOT NULL)
												and     (Player4 IS NOT NULL) 
												and     (Player5 IS NOT NULL) 
												and     (Player6 IS NULL) then $1 else Player6 end,
									Player7 = case when (Player1 IS NOT NULL) 
												and 	(Player2 IS NOT NULL)
												and     (Player3 IS NOT NULL)
												and     (Player4 IS NOT NULL) 
												and     (Player5 IS NOT NULL) 
												and     (Player6 IS NOT NULL) 
												and     (Player7 IS NULL) then $1 else Player7 end,
									Player8 = case when (Player1 IS NOT NULL) 
												and 	(Player2 IS NOT NULL)
												and     (Player3 IS NOT NULL)
												and     (Player4 IS NOT NULL) 
												and     (Player5 IS NOT NULL) 
												and     (Player6 IS NOT NULL) 
												and     (Player7 IS NOT NULL) 
												and     (Player8 IS NULL) then $1 else Player8 end
							WHERE game_id = $2`)
	if err != nil {
		log.Fatal(err)
	}
	res, err := stmt.Exec(userid, gameID)
	if err != nil {
		log.Fatal(err)
	}
	rowCnt, err := res.RowsAffected()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Player %s added to game %s, Row cnt = %d\n", userid, gameID, rowCnt)
}

//GetAll - Get All
func GetAll(context *gin.Context) {
	var gameRepo dbGamesRepo

	db := context.MustGet("DB").(*sql.DB)

	err := db.Ping()
	if err != nil {
		panic(err)
	}

	rows, err := db.Query(`SELECT g.game_id ,
	g.gameType ,
	COALESCE(g.Player1,'') ,
	COALESCE(g.Player2,'') ,
	COALESCE(g.Player3,'') ,
	COALESCE(g.Player4,'') ,
	COALESCE(g.Player5,'') ,
	COALESCE(g.Player6,'') ,
	COALESCE(g.Player7,'') ,
	COALESCE(g.Player8,'')  FROM lobby.games g;`)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() {
		var game Game
		err := rows.Scan(&game.ID, &game.GameType, &game.Player1, &game.Player2, &game.Player3, &game.Player4, &game.Player5, &game.Player6, &game.Player7, &game.Player8)
		if err != nil {
			log.Fatal(err)
		}
		gameRepo.games = append(gameRepo.games, game)
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}

	context.JSON(http.StatusOK, gameRepo.games)
}

// //Get - Get
// func Get(context *gin.Context) {
// 	game, _, err := findGame(context.Param("id"))
// 	if err == nil {
// 		context.JSON(http.StatusOK, game)
// 	} else {
// 		statusNotFound(context, err)
// 	}
// }

//Create - Create
func Create(context *gin.Context) {
	buf := make([]byte, 1024)
	num, _ := context.Request.Body.Read(buf)
	reqBody := string(buf[0:num])

	var newGameRequest createGameRequest
	err := json.Unmarshal([]byte(reqBody), &newGameRequest)
	if err != nil {
		statusBadRequest(context, err)
		return
	}

	if newGameRequest.GameType != "" && newGameRequest.UserSub != "" {
		conn, err := getStoreConnection()
		if err != nil {
			log.Fatalf("did not connect: %v", err)
		}
		defer conn.Close()

		c := pb.NewGameEventProcessorClient(conn)

		ctx, cancel := geContext.WithTimeout(geContext.Background(), time.Second)
		defer cancel()

		geGameID, err := c.CreateNewGame(ctx, &pb.CreateNewGameRequest{User: newGameRequest.UserSub})
		if err != nil {
			log.Fatalf("could not create game: %v", err)
		}

		db := context.MustGet("DB").(*sql.DB)
		stmt, err := db.Prepare("INSERT INTO lobby.games (game_id , gameType) VALUES($1, $2) RETURNING game_id")
		if err != nil {
			log.Fatal(err)
		}

		res, err := stmt.Exec(geGameID.GameID, newGameRequest.GameType)
		if err != nil {
			log.Fatal(err)
		}

		rowCnt, err := res.RowsAffected()
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("New game created: %s, %d lines modified\n", geGameID, rowCnt)

		playerJoinGame(newGameRequest.UserSub, geGameID.GameID, db)

		statusOK(context, "Game created")
	} else {
		statusBadRequest(context, err)
	}
}

// //Update -
// func Update(context *gin.Context) {
// 	gameType, err := parseForm(context)
// 	if err == nil {
// 		err := updateGame(context.Param("id"), gameType)
// 		if err == nil {
// 			statusOK(context, "Game updated.")
// 		} else {
// 			statusNotFound(context, err)
// 		}
// 	} else {
// 		statusUnprocessableEntity(context, err)
// 	}
// }

// //Delete -
// func Delete(context *gin.Context) {
// 	err := deleteGame(context.Param("id"))
// 	if err == nil {
// 		statusOK(context, "Game removed.")
// 	} else {
// 		statusNotFound(context, err)
// 	}
// }

// func datum(context *gin.Context, formKey string) string {
// 	fmt.Printf(strings.TrimSpace(context.PostForm(formKey)))
// 	return strings.TrimSpace(context.PostForm(formKey))
// }

// func parseForm(context *gin.Context) (string, error) {
// 	// Get game parameters from the POST form

// 	gameType := datum(context, "gameType")
// 	fmt.Printf(gameType)
// 	if len(gameType) == 0 {
// 		return "", fmt.Errorf("gameType required")
// 	}

// 	return gameType, nil
// }

func statusOK(context *gin.Context, report string) {
	context.JSON(http.StatusOK,
		gin.H{"status": http.StatusOK, "message": report})
}

func statusNotFound(context *gin.Context, err error) {
	context.JSON(http.StatusNotFound,
		gin.H{"status": http.StatusNotFound, "error": err})
}

func statusBadRequest(context *gin.Context, err error) {
	context.JSON(http.StatusBadRequest,
		gin.H{"status": http.StatusBadRequest, "error": err})
}

func statusUnprocessableEntity(context *gin.Context, err error) {
	context.JSON(http.StatusUnprocessableEntity,
		gin.H{"status": http.StatusUnprocessableEntity, "error": err})
}
