CREATE SCHEMA IF NOT EXISTS lobby;
CREATE SCHEMA IF NOT EXISTS game;

DROP TABLE lobby.games;

CREATE TABLE lobby.games (
	game_id VARCHAR(40) PRIMARY KEY,
	gameType VARCHAR ( 20 ) NOT NULL,
	player1 VARCHAR (40) ,
	player2 VARCHAR (40) ,
	player3 VARCHAR (40) ,
	player4 VARCHAR (40) ,
	player5 VARCHAR (40) ,
	player6 VARCHAR (40) ,
	player7 VARCHAR (40) ,
	player8 VARCHAR (40) );
	
INSERT INTO lobby.games (gameType)  VALUES  ('POKER');

DROP TABLE game.players;
CREATE TABLE game.players (
	player_id VARCHAR(40) PRIMARY KEY,
	name VARCHAR (20) NOT NULL,
	totalChips NUMERIC (24));

DROP TABLE game.players;
CREATE TABLE game.players (
	table_id serial PRIMARY KEY,
	tablePosition NUMERIC(2) NOT NULL,
	player_id VARCHAR (40) ,
	hand VARCHAR (10) ,
	chips NUMERIC(24),
	active BOOLEAN ,
	timedout BOOLEAN ,
	currentAction CHAR ,
	currentStake NUMERIC(12));

DROP TABLE game.hand;
CREATE TABLE game.hand (
	hand_id serial PRIMARY KEY,
	table_id VARCHAR (40),
	deck VARCHAR ( 350 ) NOT NULL,
	tableCards VARCHAR(40),
	gameInPlay BOOLEAN ,
	buttonPlayerIndex NUMERIC(1) ,
	currentpot NUMERIC(24) ,
	active BOOLEAN ,
	blindAmount NUMERIC(8) ,
	minimumStake NUMERIC(12));

create or replace function get_games(
)
returns table (
		game_id integer ,
	gameType VARCHAR (20) ,
	player1 VARCHAR (40) ,
	player2 VARCHAR (40) ,
	player3 VARCHAR (40) ,
	player4 VARCHAR (40) ,
	player5 VARCHAR (40) ,
	player6 VARCHAR (40) ,
	player7 VARCHAR (40) ,
	player8 VARCHAR (40) 
	) 
language plpgsql
as $$
begin
	return query 
		SELECT game_id ,
			gameType ,
			player1 ,
			player2 ,
			player3 ,
			player4 ,
			player5 ,
			player6 ,
			player7 ,
			player8  FROM lobby.games;
end; $$;